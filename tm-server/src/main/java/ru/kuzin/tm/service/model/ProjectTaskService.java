package ru.kuzin.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzin.tm.api.repository.model.IProjectRepository;
import ru.kuzin.tm.api.repository.model.ITaskRepository;
import ru.kuzin.tm.api.service.model.IProjectTaskService;
import ru.kuzin.tm.exception.entity.ProjectNotFoundException;
import ru.kuzin.tm.exception.entity.TaskNotFoundException;
import ru.kuzin.tm.exception.field.IndexIncorrectException;
import ru.kuzin.tm.exception.field.ProjectIdEmptyException;
import ru.kuzin.tm.exception.field.TaskIdEmptyException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.model.Project;
import ru.kuzin.tm.model.Task;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @Nullable final Project project;
        project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<Task> tasks;
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final List<Task> tasks;
        @Nullable Project project;
        project = projectRepository.findOneByIndex(userId, projectIndex);
        if (project == null) throw new ProjectNotFoundException();
        tasks = taskRepository.findAllByProjectId(userId, project.getId());
        for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, project.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
    }

}