package ru.kuzin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.UserDTO;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}