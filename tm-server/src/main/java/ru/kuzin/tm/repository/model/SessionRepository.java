package ru.kuzin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.api.repository.model.ISessionRepository;
import ru.kuzin.tm.model.Session;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final Session model) {
        entityManager.remove(model);
    }

}