package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.IUserOwnedRepository;
import ru.kuzin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.kuzin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}